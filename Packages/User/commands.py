import sublime
import sublime_plugin

import os
import subprocess

class GitBashHereCommand(sublime_plugin.WindowCommand):

	def run(self, **args):
		self.openBash(args["working_dir"])
		# Auto-closecommand  panel
		#self.window.run_command("hide_panel", {"panel": "output.exec"})

	def openBash(self, working_dir):
		try:
			if "$file_path" == working_dir:
				current_view = self.window.active_view()
				current_file = current_view.file_name()
				working_dir = os.path.dirname(current_file)

			bash_cmd = "C:\\Program Files (x86)\\Git\\bin\\sh.exe"
			args = [bash_cmd, "--login", "-i"]
			subprocess.Popen(args, cwd=working_dir)

		except OSError as e:
			sublime.error_message("Git bash not found" + str(e))